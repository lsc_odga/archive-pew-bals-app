import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    boundaries: {
      countyBnd: null,
      countySubBnd: null,
      tractBnd: null,
      blockGroupBnd: null,
      saBnd: null
    },
    points: {
      countyPts: null,
      countySubPts: null,
      tractPts: null,
      blockGroupPts: null,
      officePts: null
    },
    subjects: {
      hotspotsData: null
    },
    selections: {
      county: null
    }
  },

  mutations: {
    SET_SPATIAL_DATA (state, object) {
      // Boundaries
      if (object.countyBnd) { state.boundaries.countyBnd = object.countyBnd }
      if (object.countySubBnd) { state.boundaries.countySubBnd = object.countySubBnd }
      if (object.tractBnd) { state.boundaries.tractBnd = object.tractBnd }
      if (object.blockGroupBnd) { state.boundaries.blockGroupBnd = object.blockGroupBnd }
      if (object.saBnd) { state.boundaries.saBnd = object.saBnd }

      // Points
      if (object.countyPts) { state.points.countyPts = object.countyPts }
      if (object.countySubPts) { state.points.countySubPts = object.countySubPts }
      if (object.tractPts) { state.points.tractPts = object.tractPts }
      if (object.blockGroupPts) { state.points.blockGroupPts = object.blockGroupPts }
      if (object.officePts) { state.points.officePts = object.officePts }

      // Hotspot data - for initial load
      if (object.hotspotsData) { state.subjects.hotspotsData = object.hotspotsData }
    },
    SET_HOTSPOT_DATA (state, object) {
      state.subjects.hotspotsData = object
    },
    SET_COUNTY_SELECTION (state, string) {
      state.selections.county = string
    }
  },

  actions: {
    setSpatialData ({ commit }, object) {
      commit('SET_SPATIAL_DATA',  object)
    },
    setHotspotData ({ commit }, object) {
      commit('SET_HOTSPOT_DATA', object)
    },
    setCountySelection ({ commit }, string) {
      commit('SET_COUNTY_SELECTION', string)
    }
  },

  getters: {
    hotspotsDataReady: state => {
      return state.subjects.hotspotsData !== null
    },
    granteeSpatialDataReady: state => {
      return state.boundaries.saBnd !== null && state.points.officePts !== null
    },
    countySpatialDataReady: state => {
      return state.boundaries.countyBnd !== null && state.points.countyPts !== null
    },
    countySubSpatialDataReady: state => {
      return state.boundaries.countySubBnd !== null && state.points.countySubPts !== null
    },
    tractSpatialDataReady: state => {
      return state.boundaries.tractBnd !== null && state.points.tractPts !== null
    },
    blockGroupSpatialDataReady: state => {
      return state.boundaries.blockGroupBnd !== null && state.points.blockGroupPts !== null
    }
  },

  modules: {
  }
})
