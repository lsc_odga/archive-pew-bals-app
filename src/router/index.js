import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

function lazyLoad(view){
  return() => import(`@/views/${view}.vue`)
}

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: lazyLoad('Home')
    },
    {
      path: '/hotspots',
      name: 'Hotspots',
      component: lazyLoad('Hotspots')
    },
    {
      path: '/pha',
      name: 'PHA',
      component: lazyLoad('PHA')
    },
    {
      path: '/trends',
      name: 'Trends',
      component: lazyLoad('Trends')
    },
    {
      path: '/about',
      name: 'About',
      component: lazyLoad('About')
    }
  ]
})

export default router
