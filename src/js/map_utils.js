const d3_scale = require('d3-scale');
const d3_array = require('d3-array');

export function calculateLinearColorScale(data, selectedVar, colorArray) {
	return d3_scale.scaleLinear()
		.domain([
			d3_array.min(data, function(d) { return d[selectedVar]; }),
			d3_array.max(data, function(d) { return d[selectedVar]; })
		])
		.range(colorArray);
}

export function setChoroplethColor(map, layer, data, selectedVar, colorScale) {

  // GL match expression to assign color to the matching county ID (the indicated spatial data variable)
  let matchExpression = ['match', ['get', 'GEOID']];

  //Pull the right value out the D3 color scale for each geo feature. Build up the match expression.
  data.forEach(function (d) {
    d.fips = String(d.fips);
    let color = colorScale(d[selectedVar]);
    matchExpression.push(d['fips'], color);
  });

  //Last value is default, used where there is no data.
  matchExpression.push('rgba(0, 0, 0, 0)');

  //Set the paint color for each feature
  map.setPaintProperty(
    layer,
    'fill-color',
    matchExpression
  );

}